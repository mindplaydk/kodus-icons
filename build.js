const dir = require("node-dir");
const path = require("path");
const fs = require("fs");
const webfont = require('webfont').default;
const gzip_size = require("gzip-size");
const md5_file = require('md5-file');

const registry = require("./_registry");

const ROOT_PATH = registry.ROOT_PATH;

let icons = registry.load();

validate();
build_assets();

function validate() {
    let names = {};

    for (let icon of icons) {
        if (names[icon.name]) {
            //throw Error(`duplicate name: ${icon.name}\nconflicting entries:\n  ${JSON.stringify(names[icon.name])}\n  ${JSON.stringify(icon)}\nplease correct conflicts (in the editor) and try again`);
            console.warn(`duplicate name: ${icon.name}\nconflicting entries:\n  ${JSON.stringify(names[icon.name])}\n  ${JSON.stringify(icon)}\nplease correct conflicts (in the editor) and try again`);
        }
    }
}

function build_assets() {
    const file_map = {};

    for (let icon of icons) {
        icon.file = path.join(ROOT_PATH, icon.file);
        
        file_map[icon.file] = icon;
    }

    webfont({
        fontName: "kodus-icons",
        formats: ["woff"],
        files: icons.map(icon => icon.file),
        template: `${ROOT_PATH}/icons.css.njk`,
        glyphTransformFn: (glyph) => {
            glyph.name = file_map[glyph.path].name;
        }
    }).then(result => {
        const woff_path = `${ROOT_PATH}/dist/icons.woff`;

        fs.writeFileSync(woff_path, result.woff);

        console.log(`created: ${woff_path} (${gzip_size.sync(result.woff)} bytes gzipped)`);

        const css_path = `${ROOT_PATH}/dist/icons.css`;

        fs.writeFileSync(css_path, result.styles);

        console.log(`created: ${css_path} (${gzip_size.sync(result.styles)} bytes gzipped)`);
    });
}
